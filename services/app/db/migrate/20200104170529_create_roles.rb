class CreateRoles < ActiveRecord::Migration[5.1]
  def change
    create_table :roles do |t|
      t.string :name
    end
    Role.create :name => 'admin'
    Role.create :name => 'user'
  end

end
