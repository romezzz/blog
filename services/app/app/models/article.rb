class Article < ApplicationRecord
  has_many :comments, dependent: :destroy
  belongs_to :category

  validates :title, :text, presence: true
  validates :title, length: { minimum: 5, maximum: 250 }
end
