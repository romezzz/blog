class Admin::BaseController < ApplicationController
  # before_action :authenticate_user!
  before_action :check_admin

  protected

  def check_admin
    redirect_to home_path unless current_user.role?('admin')
  end
end
