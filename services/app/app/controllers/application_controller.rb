class ApplicationController < ActionController::Base
  include Flash
  protect_from_forgery with: :exception
  helper_method :menu_categories

  def menu_categories
    @menu_categories ||= Category.all
  end

  def current_user
    super || Guest.new
  end
end
