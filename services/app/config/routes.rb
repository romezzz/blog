Rails.application.routes.draw do

  devise_for :users, :controllers => { :omniauth_callbacks => "callbacks" }

  root 'welcome#index', as: 'home'

  resources :articles do
    resources :comments, only: [:index, :show]
  end

  resources :categories, only: [:show]

  get 'about' => 'pages#about'

  namespace :admin do
    resources :categories, except: [:show]
    resources :articles, except: [:index, :show]
  end
end
